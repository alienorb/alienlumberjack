# AlienLumberjack

AlienLumberjack is a small collection of useful classes to use along with the fantastic [Cocoa Lumberjack](https://github.com/robbiehanson/CocoaLumberjack) framework. 

## Classes
   
**AOLoggingManager**  
A convenience class that takes care of setting up a few basic loggers using our custom formatters. Simply invoke *[AOLoggingManager setupDefaultLoggers]* in your *applicationDidFinishLaunching:* method to establish the loggers.

**AOFileAndLineNumberLogFormatter**  
A custom log formatter class which will cause messages to be logged in the following manner: *[MySourceFileName:LineNumber]: my log message here*.

**AODateTimeFileAndLineNumberLogFormatter**  
A custom log formatter class which will cause messages to be logged in the following manner: *year-month-day hour:minute:second:millisecond [MySourceFileName:LineNumber]: my log message here*.

## Requirements 

This code has been written for the modern Objective-C runtime, using such features as automatic reference counting (ARC). It thusly requires the use of the LLVM compiler version 5.0 or later, and must target either iOS 7 (or later) or Mac OS X 10.9 (or later).

## License

The MIT License

Copyright (c) 2011-2013 Alien Orb Software LLC. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.          
